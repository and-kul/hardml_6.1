from flask import Flask, jsonify

import application
from application import db, helpers
from time import sleep


app = Flask(__name__)


# @app.route('/')
# def hello_world():
#     return f"Hello, it's me! {application.REPLICA_NAME=} {application.REPLICA_HOST=} {application.REPLICA_PORT=}"


@app.route('/return_secret_number')
def return_secret_number():
    # sleep(1)
    return jsonify(secret_number=db.secret_number)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)
    # app.run()
