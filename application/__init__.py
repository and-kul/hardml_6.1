import sys

from application import db, helpers
# import os

# REPLICA_NAME = os.getenv('REPLICA_NAME', default='web_app_1')
# REPLICA_HOST = os.getenv('REPLICA_HOST', default='65.108.92.116')
# REPLICA_PORT = int(os.getenv('REPLICA_PORT', default='8000'))


# def init_redis(host: str, port: int):
#     if db.redis_connection is None:
#         # Еще не создан - создайте
#         db.redis_connection = redis.Redis(host=host, port=port, password='***', decode_responses=True)
#         print('Redis is initialized')
#
#     else:
#         print('strange in init_redis')
#         pass
#
#
# def register_app():
#     db.redis_connection.rpush('web_app', REPLICA_NAME)
#     db.redis_connection.hmset(REPLICA_NAME, {'host': REPLICA_HOST, 'port': REPLICA_PORT})
#
#
# def unregister_app():
#     print('inside unregister_app')
#     db.redis_connection.lrem('web_app', 0, REPLICA_NAME)
#     db.redis_connection.delete(REPLICA_NAME)
#
# def signal_handler(sig, frame):
#     print(f'YOU CALLED ME WITH {sig}')
#     # Разрегистрируемся в реджистри
#     unregister_app()
#
#     # Отключаемся от базы
#     if db.redis_connection is not None:
#         db.redis_connection.close()
#     sys.exit()
#
#
# signal.signal(signal.SIGTERM, signal_handler)
# signal.signal(signal.SIGINT, signal_handler)
# # signal.signal(signal.SIGKILL, signal_handler) # SIGKILL мы отловить не можем
#
# print('hello this is main')
# db.secret_number = helpers.get_secret_number()
# init_redis(host='65.108.92.116', port=6379)
# register_app()
