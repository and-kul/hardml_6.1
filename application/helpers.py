import requests
from time import sleep


def get_secret_number():
    URL ='https://lab.karpov.courses/hardml-api/module-5/get_secret_number'
    response = requests.get(URL)
    while response.status_code != 200:
        sleep(1)
        response = requests.get(URL)
    return response.json()['secret_number']
